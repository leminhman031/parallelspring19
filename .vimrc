set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'https://github.com/Valloric/YouCompleteMe.git'  
Plugin 'https://github.com/ajh17/VimCompletesMe.git'
Plugin 'vim-airline/vim-airline'
Plugin 'https://github.com/tomasiser/vim-code-dark.git'
Plugin 'https://github.com/tpope/vim-fugitive.git' 
Plugin 'https://github.com/scrooloose/nerdcommenter.git' 
Plugin 'https://github.com/dunstontc/vim-vscode-theme.git' 
Plugin 'NLKNguyen/papercolor-theme'
Plugin 'https://github.com/vim-airline/vim-airline-themes.git' 
Plugin 'https://github.com/skywind3000/asyncrun.vim.git' 
Plugin 'https://github.com/johngrib/vim-game-snake.git'
Plugin 'https://github.com/blueyed/vim-diminactive.git' 
Plugin 'https://github.com/vim-scripts/Solarized.git' 
Plugin 'https://github.com/mswift42/vim-themes.git'
Plugin 'https://github.com/MattesGroeger/vim-bookmarks.git'
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line






""""""""""""""""""  KEY MAPPINGS """"""""""""""""""""""""  

"To resize split
nnoremap <C-Left> <C-W>>
nnoremap <C-Right> <C-W><

nnoremap <C-Down>  :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <C-Up>  :exe "resize " . (winheight(0) * 2/3)<CR>

" Move between buffers
"nmap <silent> <A-Up> :wincmd k<CR>
"nmap <silent> <A-Down> :wincmd j<CR>
"nmap <silent> <A-Left> :wincmd h<CR>
"nmap <silent> <A-Right> :wincmd l<CR

"Move between splits 


"nnoremap <C-J> <C-W><C-J>
"nnoremap <C-K> <C-W><C-K>
"nnoremap <C-L> <C-W><C-L>
"nnoremap <C-H> <C-W><C-H>

nnoremap <S-Down> <C-W><C-J>
nnoremap <S-Up> <C-W><C-K>
nnoremap <S-Right> <C-W><C-L>
nnoremap <S-Left> <C-W><C-H>

"Move between tabs 

map <A-Up>   :tabr<cr>

map <A-Down> :tabl<cr>

map <A-Left> :tabp<cr>

map <A-Right> :tabn<cr>




"Buffer maps
:nnoremap <C-n> :bnext<CR>
:nnoremap <C-p> :bprevious<CR>

"Move tabs 

noremap <C-t><left>    :-tabmove<cr>
noremap <C-t><right>    :+tabmove<cr>


"For ShockTesting 
"map <F12> :!sh -xc 'clear && gfortran shocktest.f95 && ./a.out' <enter>

"map <F8> :!sh -xc 'gnuplot shockplot &&  eog --fullscreen SHOCKRESULT.png ' <enter>



autocmd BufNewFile,BufRead ~/sonov2/*  map <F5> :!sh -xc 'clear && ./clean.sh' <enter>
autocmd BufNewFile,BufRead ~/sonov2/*  map <F6> :!sh -xc 'clear && ./restart.sh ' <enter>
autocmd BufNewFile,BufRead ~/sonov2/*  map <F7> :!sh -xc './createFRAMES.sh' <enter>
autocmd BufNewFile,BufRead ~/sonov2/*  map <F8> :!sh -xc 'eog --fullscreen test/tecplotoutput/frames_movies/*.png ' <enter>



""""""""""""""""""  THEMES  """"""""""""""""""""""""  
set background=dark 
colo codedark 
set cursorline
""""""""""""""" AIRLINE STUFF """"""""""""""""""""""

let g:airline_theme='codedark'  
let g:airline_section_z = '%t'


"""""""""""""""" FORTRAN STUFF """"""""""""""""""""""""





" Ensure correct highlighting for
" Fortran free-form source code
" and turn syntax highlighting on
let fortran_free_source=1
let fortran_do_enddo=1
filetype plugin indent on
syntax on

" Turn on line numbers and
" row/column numbers
set nu
set ruler

" Make vim echo commands as they
" are being entered.
set showcmd

" Set tabstops to two spaces
" and ensure tab characters are
" expanded into spaces.
set smarttab
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
" Fix backspace key
set bs=2
set tw=80 
" Set up searching so
" that it jumps to matches
" as the word is being
" entered and is case-insensitive
set incsearch
set ignorecase
set smartcase
set autoindent 


let g:airlinetogglewhitespace = 0
hi MatchParen cterm=bold ctermbg=yellow ctermfg=red
let g:airline#extensions#branch#enabled = 1
let g:airline_section_c = '%{strftime("%I:%M %p")}'
let g:netrw_browse_split = 3
"let g:netrw_list_hide = '^\./$'
let g:netrw_banner=0
"let g:netrw_hide = 0
"

"
"let g:diminactive_use_syntax = 1
"let g:diminactive_enable_focus = 1
let g:diminactive_use_colorcolumn = 1

set clipboard=unnamed

nmap <C-_> <leader>c<Space>
 vmap <C-_> <leader>c<Space>




"function! MyTabLine()
    "let s = ''
    "for i in range(tabpagenr('$'))
        "let tabnr = i + 1
        "let winnr = tabpagewinnr(tabnr)
        "let buflist = tabpagebuflist(tabnr)
        "let bufnr = buflist[winnr - 1]
        "let bufname = fnamemodify(bufname(bufnr), ':t')
        "let s .= '%' . tabnr . 'T'
        "let s .= (tabnr == tabpagenr() ? '%#TabLineSel#' : '%#TabLine#')
        "let s .= ' ' . tabnr
        "let s .= empty(bufname) ? ' [No Name] ' : ' ' . bufname . ' '
    "endfor
    "let s .= '%#TabLineFill#'
    "return s
"endfunction
"set showtabline=1
"set tabline=%!MyTabLine()

hi TabLineSel term=bold ctermfg=White ctermbg=32
"hi TabLine ctermfg=White ctermbg=Black  

highlight BookmarkSign ctermbg=NONE ctermfg=160
highlight BookmarkLine ctermbg=194 ctermfg=NONE
let g:bookmark_sign = '♥'
let g:bookmark_highlight_lines = 1


"nnoremap <C-c> :!clear && gfortran % -Wall -Wextra -Wconversion -o %.out && ./%.out<CR>



nnoremap <F2> :!clear && gfortran % -Wall -Wextra -Wconversion -o %.out <CR>
nnoremap <F3> :!clear && gfortran -fopenmp  % -Wall -Wextra -Wconversion -o %.out <CR>
nnoremap <F4> :! ./%.out<CR>
"hi CursorLine term=bold cterm=NONE gui=NONE ctermbg=254


"hi ColorColumn ctermbg=254 guibg=#eee8d5

highlight CursorLineNr ctermfg=black ctermbg=yellow
filetype plugin on 
filetype indent on 
let g:tex_flavor='latex'

set laststatus=2
"set statusline=[%n]\ %<%f%h%m
